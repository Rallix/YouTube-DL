﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace YouTubeDL.YouTube
{
    /// <summary> All tasks related to YouTube downloading. </summary>
    static class Download
    {
        /// <summary> Calls the <see langword="youtube-dl"/> console command with given arguments. </summary>
        /// <param name="url"> The video url. </param>
        /// <param name="outputEvent"> The event to call when output is received. </param>
        /// <param name="args"> Arguments given to the command. </param>
        static async Task<string> YT(string url, Action<object, DataReceivedEventArgs> outputEvent, params string[] args)
        {
            string output = string.Empty;
            // All arguments, space-separated, ending with the URL
            string arguments = string.Join(' ', args) + $" {url}";
            // Start the child process
            using var yt = new Process
            {
                    StartInfo =
                    {
                            RedirectStandardOutput = true, // Redirect the output stream of the process
                            UseShellExecute = false,       // Specify exacutable in PATH directly
                            CreateNoWindow = true,         // Hide the console window
                            FileName = "youtube-dl.exe",
                            Arguments = arguments
                    }
            };
            if (outputEvent != null) yt.OutputDataReceived += delegate(object sender, DataReceivedEventArgs data) { outputEvent(sender, data); };
            yt.Start();
            if (outputEvent == null) output = yt.StandardOutput.ReadToEnd().Trim();
            else yt.BeginOutputReadLine();
            await yt.WaitForExitAsync();
            return output;
        }

        /// <summary> Downloads a title of a video. </summary>
        /// <param name="url"> The URL of the video. </param>
        /// <returns> The title of the video as displayed on YouTube. </returns>
        public static async Task<string> GetTitle(string url)
        {
            // youtube-dl --skip-download --get-title
            return await YT(url, null, "--skip-download", "--get-title");
        }
        
        public static async Task DownloadVideo(string url, string folder = "", Action<int> progressCallback = null)
        {
            void OutputEvent(object sender, DataReceivedEventArgs data)
            {
                string line = data.Data;
                if (!string.IsNullOrWhiteSpace(line) && line.StartsWith("[download]") && line.Contains("%"))
                {
                    string numbers = Regex.Match(line, @"]\s*(\d+\.\d+)%").Groups[1].Value;
                    if (float.TryParse(numbers, NumberStyles.Number, CultureInfo.InvariantCulture, out float percentage))
                    {
                        int roundedPercentage = (int) Math.Round(percentage, 0);
                        progressCallback?.Invoke(roundedPercentage);
                    }
                }
            }

            await YT(url, OutputEvent,
                     "--extract-audio --audio-format mp3",
                     string.IsNullOrWhiteSpace(folder) ? "-o \"%(title)s.%(ext)s\"" : $"-o \"{folder}\\%(title)s.%(ext)s\"",
                     "--newline");
        }

        /// <summary> Waits asynchronously for the process to exit. </summary>
        /// <param name="process">The process to wait for cancellation.</param>
        /// <param name="cancellationToken">A cancellation token. If invoked, the task will return 
        /// immediately as canceled.</param>
        /// <returns>A Task representing waiting for the process to end.</returns>
        static Task WaitForExitAsync(this Process process, CancellationToken cancellationToken = default)
        {
            var tcs = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => tcs.TrySetResult(null);
            if (cancellationToken != default) cancellationToken.Register(tcs.SetCanceled);
            return tcs.Task;
        }
    }
}
