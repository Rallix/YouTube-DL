﻿using System.Threading.Tasks;

namespace YouTubeDL.YouTube
{
    /// <summary> A representation of a YouTube video. </summary>
    class Video
    {
        public string Url { get; }

        public string Title => Task.Run(() => Download.GetTitle(Url)).Result;

        public Video(string url)
        {
            Url = url ?? string.Empty;
        }
    }
}
