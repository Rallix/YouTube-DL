﻿using System;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;

namespace YouTubeDL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary> Updates the title of a song. </summary>
        async void TitleButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(LinkTextBox.Text.Trim()))
            {
                string title = await YouTube.Download.GetTitle(LinkTextBox.Text);
                TitleLabel.Content = title;
            }
            else
            {
                MessageBox.Show("Není možné zobrazit název skladby, když nebyl zadaný žádný odkaz.");
            }
        }

        /// <summary> Opens YouTube.com or the used song. </summary>
        void YTLogoImage_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            string link = "https://www.youtube.com";
            if (!string.IsNullOrWhiteSpace(LinkTextBox.Text)) link = LinkTextBox.Text;
            
            using var ytLink = new Process
            {
                    StartInfo =
                    {
                            RedirectStandardOutput = true, // Redirect the output stream of the process
                            UseShellExecute = false,       // Specify exacutable in PATH directly
                            CreateNoWindow = true,         // Hide the console window
                            FileName = "cmd",
                            Arguments = "/C start" + " " + link
                    }
            };
            ytLink.Start();

            e.Handled = true;
        }

        /// <summary> Will show or hide the guide. </summary>
        void ShowHideGuide(object sender, RoutedEventArgs e)
        {
            const int groupSize = 275;
            if (GuideGroupBox.Visibility == Visibility.Visible)
            {
                GuideGroupBox.Visibility = Visibility.Collapsed;
                GuideButton.Content = "Zobrazit návod";
                Height -= groupSize;
            }
            else
            {
                GuideGroupBox.Visibility = Visibility.Visible;
                GuideButton.Content = "Skrýt návod";
                Height += groupSize;
            }
        }

        void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            using var ytLink = new Process
            {
                    StartInfo =
                    {
                            RedirectStandardOutput = true, // Redirect the output stream of the process
                            UseShellExecute = false,       // Specify exacutable in PATH directly
                            CreateNoWindow = true,         // Hide the console window
                            FileName = "cmd",
                            Arguments = "/C start" + " " + e.Uri.AbsoluteUri
                    }
            };
            ytLink.Start();
            e.Handled = true;
        }


        void OutputFolderBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            using var fsd = new CommonOpenFileDialog
            {
                    EnsureReadOnly = true,
                    IsFolderPicker = true,
                    AllowNonFileSystemItems = false,
                    Multiselect = false,
                    // InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Title = "Výstupní složka"
            };

            if (fsd.ShowDialog() == CommonFileDialogResult.Ok)
            {
                OutputFolderTextBox.Text = fsd.FileName;
            }
        }

        async void DownloadButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(LinkTextBox.Text))
            {
                MessageBox.Show("Nebyl zadaný žádný odkaz ke stažení.");
                return;
            }

            DownloadProgressBar.Value = 0;

            void OnYTProgress(int progress)
            {
                // Clamp --> there's also conversion after downloading
                DownloadProgressBar.Dispatcher?.Invoke(() => DownloadProgressBar.Value = Math.Clamp(progress, 0, 75));
            }

            await YouTube.Download.DownloadVideo(LinkTextBox.Text.Trim(), OutputFolderTextBox.Text.Trim(), OnYTProgress);

            LinkTextBox.Text = string.Empty;
            TitleLabel.Content = string.Empty;
            DownloadProgressBar.Value = 0;
            System.Media.SystemSounds.Asterisk.Play();
        }
    }
}
